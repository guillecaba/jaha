import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//Componentes
import { AgregarViajeComponent } from '../pages/agregarViaje/agregarViaje.component';
import { ViajesComponent } from '../pages/viajes/viajes.component';

//Servicios
import { ViajesService } from './services/viajes.service';
import { HttpClientModule } from '@angular/common/http';






@NgModule({
  declarations: [
    MyApp,
    AgregarViajeComponent,
    ViajesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AgregarViajeComponent,
    ViajesComponent
  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ViajesService
  ]
})
export class AppModule {}
