import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable()
export class ViajesService{
    
    private url = "https://jaha-7af9c.firebaseio.com/";
    viajesURL:string='https://jaha-7af9c.firebaseio.com/Viajes.json';
    constructor( private http:HttpClient ) {
        
     }

    consultar(){
        let r:Object[]=[];
        try{
          //<<<<<<<this.http.head("Content-type = aplication/json")
          let a = this.http.get(this.url + "Viajes.json");
          a.forEach((x) => {
            for(let i in x){
              x[i].id=i;
              console.log(x[i]);
              r.push(x[i]);
    
            }
          })
          console.log(r);
          return r;
        }catch (e){
          console.log("No se pudo")
        }
        return null;
      }

    

    nuevoViaje(viaje:any){
        let body = JSON.stringify(viaje);
        let headers = new HttpHeaders({
            'Content-Type':'application/json'
          });

        return this.http.post(this.viajesURL,body,{ headers }).
        pipe(map(res=>{
            console.log(res);
            
            return res;
        }))
    }
    
}