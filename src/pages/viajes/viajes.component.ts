import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ViajesService } from '../../app/services/viajes.service';
import { AgregarViajeComponent } from '../agregarViaje/agregarViaje.component';



@Component({
  selector: 'app-viajes',
  templateUrl: 'viajes.component.html'
})
export class ViajesComponent {
  Ubicaciones:string; 
  items: any[];
  constructor(public navCtrl: NavController,public _viajesServices: ViajesService) {
    this.Ubicaciones = 'Lista';
    this.items=this._viajesServices.consultar();
    
  }
 
  agregar(){
    this.navCtrl.push(AgregarViajeComponent);
  }
  actualizar(){
    this.items = this._viajesServices.consultar();
  }
  
}
