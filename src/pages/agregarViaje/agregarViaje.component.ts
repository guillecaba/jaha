import { Component } from '@angular/core';
import { ViajesService } from '../../app/services/viajes.service';
import { ToastCmp, ToastController, NavController } from 'ionic-angular';


@Component({
    selector: 'app-agregarViaje',
    templateUrl: 'agregarViaje.component.html',
  })
  export class AgregarViajeComponent{
    private viaje:any ={
      desde:"",
      hasta:"",
      fecha:"",
      hora:"",
      asientos:"",
      costo:"",
      referencias:"",

    };
    nombre:string;
    
  
    constructor(private _viajesService:ViajesService,
      public toastCtrl: ToastController,
      public navCtrl:NavController) {  }

    guardar(){
      console.log(this.viaje);
      this._viajesService.nuevoViaje(this.viaje)
      .subscribe(data=>{
        console.log(data);
        this.presentToast();
        this.navCtrl.pop();
        
      })
    }
    presentToast() {
      const toast = this.toastCtrl.create({
        message: 'Se ha creado correctamente',
        duration: 3000
      });
      toast.present();
    }
  }
  